package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Note;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class StockageNotesDataBase implements Stockage<Note> {


    @Override
    public void create(Note element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "INSERT INTO notesOGE (idEtudiant, idRessource, note) VALUES (?, ?, ?)";

        try {

            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setInt(1, element.getEtudiant().getIdEtudiant());
            statement.setInt(2, element.getRessource().getIdRessource());
            statement.setInt(3, element.getNote());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Note element) {

        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "UPDATE notesOGE SET note = ? WHERE idNote = ?";

        try {

            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setInt(1, element.getNote());
            statement.setInt(2, element.getIdNote());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {

        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "DELETE FROM notesOGE WHERE idNote = ?";

        try {

            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setInt(1, id);

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Note getById(int id) {

        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "SELECT * FROM notesOGE WHERE idNote = ?";
        Note note = new Note();

        try {

            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {

                note.setIdNote(resultSet.getInt("idNote"));
                Etudiant etudiant = new Etudiant();
                etudiant.setIdEtudiant(resultSet.getInt("idEtudiant"));

                note.setEtudiant(etudiant);

                Ressource ressource = new Ressource();
                ressource.setIdRessource(resultSet.getInt("idRessource"));
                note.setRessource(ressource);

                note.setNote(resultSet.getInt("note"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return note;
    }

    @Override
    public List<Note> getAll() {

        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "SELECT * FROM NOTESOGE";
        List<Note> listeN = new ArrayList<>();

        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(requete);

            while (resultSet.next()) {

                Note note = new Note();
                note.setIdNote(resultSet.getInt("idNote"));

                Etudiant etudiant = new Etudiant();
                etudiant.setIdEtudiant(resultSet.getInt("idEtudiant"));
                note.setEtudiant(etudiant);

                Ressource ressource = new Ressource();
                ressource.setIdRessource(resultSet.getInt("idRessource"));
                note.setRessource(ressource);

                note.setNote(resultSet.getInt("note"));

                listeN.add(note);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listeN;
    }
}
