package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Etudiant;
import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageEtudiantDatabase implements Stockage<Etudiant> {

    // Il faut simplement faire pareil que dans StockageRessourceDatabase
    // On test et si ca marche je me fais le cul
    @Override
    public void create(Etudiant element) {
        Connection connection = SQLUtils.getInstance().getConnection(); // On recupere la connection
        String requete = "INSERT INTO ETUDIANTSOGE (nom, prenom, idRessourceFavorite) VALUES (?, ?, ?)";

        try  (
                PreparedStatement statement = connection.prepareStatement(requete);
        )
                {


            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());

            Ressource ressource = element.getRessourceFavorite();
            System.out.println(ressource);
            System.out.println(ressource.getIdRessource());

            statement.setInt(3, ressource.getIdRessource());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void update(Etudiant element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "UPDATE etudiantsOGE SET nom = ?, prenom = ?, idRessourceFavorite = ? WHERE idEtudiant = ?";

        try (
                PreparedStatement statement = connection.prepareStatement(requete);

                ) {

            statement.setString(1, element.getNom());
            statement.setString(2, element.getPrenom());

            Ressource ressource = element.getRessourceFavorite();
            statement.setInt(3, ressource.getIdRessource());
            statement.setInt(4, element.getIdEtudiant());

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteById(int id) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "DELETE FROM etudiantsOGE WHERE idEtudiant = ?";

        try (
                PreparedStatement statement = connection.prepareStatement(requete);

                ){
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Etudiant getById(int id) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "SELECT * FROM etudiantsOGE WHERE idEtudiant = ?";
        Etudiant etudiant = new Etudiant();

        try (
                PreparedStatement statement = connection.prepareStatement(requete);

                ){
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            etudiant.setIdEtudiant(resultSet.getInt("idEtudiant"));
            etudiant.setNom(resultSet.getString("nom"));
            etudiant.setPrenom(resultSet.getString("prenom"));

            Ressource resourceFavorite = new Ressource();
            resourceFavorite.setIdRessource(resultSet.getInt("idRessourceFavorite"));

            //System.out.println(resourceFavorite);
            etudiant.setRessourceFavorite(resourceFavorite);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return etudiant;
    }

    @Override
    public List<Etudiant> getAll() {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "SELECT * FROM etudiantsOGE";
        List<Etudiant> listeE = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(requete);

            while (resultSet.next()) {
                Etudiant etudiant = new Etudiant();
                etudiant.setIdEtudiant(resultSet.getInt("idEtudiant"));
                etudiant.setNom(resultSet.getString("nom"));
                etudiant.setPrenom(resultSet.getString("prenom"));

                int idRessourceFavorite = resultSet.getInt("idRessourceFavorite");
                System.out.println("idRessourceFavorite récupéré : " + idRessourceFavorite);

                StockageRessourceDatabase stockageRessourceDatabase = new StockageRessourceDatabase();
                Ressource ressourceFavorite = stockageRessourceDatabase.getById(idRessourceFavorite);


                etudiant.setRessourceFavorite(ressourceFavorite);

                System.out.println("ressourceFavorite : " + ressourceFavorite);
                listeE.add(etudiant);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return listeE;
    }

}
